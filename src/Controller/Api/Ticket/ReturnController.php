<?php

namespace App\Controller\Api\Ticket;

use App\Entity\Flight;
use App\Entity\Ticket;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReturnController extends AbstractController
{
    /** @var EntityManagerInterface $em */
    private $em;

    /**
     * ReturnController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("api/v1/callback/events/tickets/return", name="cancel_ticket", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function executeAction(Request $request): Response
    {
        $flightId = $request->get('flightId');
        $place = $request->get('place');

        /** @var Flight $flight */
        $flight = $this->em->getRepository(Flight::class)
            ->findOneBy(['id' => $flightId]);

        if (!$flight) {
            return new Response('Ошибка параметра flight', 404);
        }

        if ($flight->getStatus() !== Flight::FLIGHT_STATUS_IN_PROGRESS) {
            return new Response('Регистрация невозможна', 404);
        }

        /** @var Ticket $ticket */
        $ticket = $this->em->getRepository(Ticket::class)
            ->findOneBy(['flight' => $flight, 'place' => $place]);

        if (!$ticket || $ticket->getStatus() !== Ticket::PLACE_STATUS_PURCHASED) {
            return new Response('Место не существует или не забронировано', 404);
        }

        $ticket->setStatus(Ticket::PlACE_STATUS_AVAILABLE);
        $ticket->setName('');
        $ticket->setEmail('');
        $this->em->persist($ticket);
        $this->em->flush();

        return new JsonResponse('Ok');
    }
}