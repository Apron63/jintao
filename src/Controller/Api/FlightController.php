<?php

namespace App\Controller\Api;

use App\Entity\Flight;
use App\Entity\Ticket;
use App\Manager\FlightCancelledMailManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FlightController extends AbstractController
{
    /** @var EntityManagerInterface $em */
    private $em;
    /** @var FlightCancelledMailManager $cancelledMailManager */
    private $cancelledMailManager;

    /**
     * FlightController constructor.
     * @param EntityManagerInterface $em
     * @param FlightCancelledMailManager $cancelledMailManager
     */
    public function __construct(EntityManagerInterface $em, FlightCancelledMailManager $cancelledMailManager)
    {
        $this->em = $em;
        $this->cancelledMailManager = $cancelledMailManager;
    }

    /**
     * @Route("api/v1/callback/events/flights/{id<\d+>}/cancel", name="flight_cancel", methods={"POST"})
     * @param int $id
     * @return Response
     */
    public function executeAction(int $id): Response
    {
        $flight = $this->em->getRepository(Flight::class)
            ->findOneBy(['id' => $id]);

        if (!$flight) {
            return new Response('Ошибка параметра', 404);
        }

        /** @var Ticket $ticket */
        foreach ($flight->getTickets() as $ticket) {
            $this->cancelledMailManager->sendMailFlightCancelled($ticket->getEmail());
        }

        return new JsonResponse('Ok');
    }
}