<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Ticket
 *
 * @ORM\Entity
 * @ORM\Table(name="ticket")
 * @ORM\Entity(repositoryClass="App\Repository\TicketRepository")
 *
 * @ApiResource(
 *     itemOperations={
 *         "get",
 *         "reserve"={"route_name"="reserve_ticket"},
 *         "cancel"={"route_name"="cancel_ticket"},
 *         "purchase"={"route_name"="purchase_ticket"},
 *         "return"={"route_name"="return_ticket"}
 *  })
 */
class Ticket
{
    public const PlACE_STATUS_AVAILABLE = 0;
    public const PLACE_STATUS_RESERVED = 1;
    public const PLACE_STATUS_PURCHASED = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     *
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     *
     */
    private $email;

    /**
     * @Assert\Type(type="App\Entity\Flight")
     * @var Flight
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Flight")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="flight_id", referencedColumnName="id")
     * })
     */
    private $flight;

    /**
     * @var int
     *
     * @ORM\Column(name="place", type="integer")
     */
    private $place;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Ticket
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Ticket
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set flight
     *
     * @param Flight $flight
     *
     * @return Ticket
     */
    public function setFlight($flight)
    {
        $this->flight = $flight;

        return $this;
    }

    /**
     * Get flight
     *
     * @return Flight
     */
    public function getFlight()
    {
        return $this->flight;
    }

    /**
     * Set place
     *
     * @param int $place
     *
     * @return Ticket
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return int
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set status
     *
     * @param int $status
     *
     * @return Ticket
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
}
